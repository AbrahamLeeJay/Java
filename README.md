# Java
Java常用工具封装

2020年03月17日 Java SE 14发布

2020年3月17日，JDK14正式发布。
JDK14将在4月和7月收到安全更新，然后由9月到期的非LTS版本的JDK 15取代。JDK14包括16项新功能，例如JDK Flight Recorder事件流，模式匹配和开关表达式等特征。
从JDK9之后，Oracle采用了新的发布周期：每6个月发布一个版本，每3年发布一个LTS版本。JDK14是继JDK9之后发布的第四个版本， 该版本为非LTS版本，最新的LTS版本为JDK11。
下载地址
http://jdk.java.net/14/
JDK 14
JDK 14 is the open-source reference implementation of version 14 of the Java SE Platform as specified by by JSR 389 in the Java Community Process.

JDK 14 reached General Availability on 17 March 2020. Production-ready binaries under the GPL are available from Oracle; binaries from other vendors will follow shortly.

The features and schedule of this release were proposed and tracked via the JEP Process, as amended by the JEP 2.0 proposal. The release was produced using the JDK Release Process (JEP 3).

Features
305:	Pattern Matching for instanceof (Preview)
343:	Packaging Tool (Incubator)
345:	NUMA-Aware Memory Allocation for G1
349:	JFR Event Streaming
352:	Non-Volatile Mapped Byte Buffers
358:	Helpful NullPointerExceptions
359:	Records (Preview)
361:	Switch Expressions (Standard)
362:	Deprecate the Solaris and SPARC Ports
363:	Remove the Concurrent Mark Sweep (CMS) Garbage Collector
364:	ZGC on macOS
365:	ZGC on Windows
366:	Deprecate the ParallelScavenge + SerialOld GC Combination
367:	Remove the Pack200 Tools and API
368:	Text Blocks (Second Preview)
370:	Foreign-Memory Access API (Incubator)
Schedule
2020/03/17		General Availability